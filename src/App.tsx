/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import {StyleSheet} from 'react-native';
//Navigation
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Login from './Main Tabs/Login';
import Mainscreen from './Main Tabs/Mainscreen';
import CreateNew from './subTabs/CreateNew';

export type RootStackParamList = {
  Main: undefined;
  Login: undefined;
  AcademicCalender: undefined;
  Notice: undefined;
  CreateNew: undefined;
};

const stack = createNativeStackNavigator<RootStackParamList>();

function App(): JSX.Element {
  return (
    <NavigationContainer>
      <stack.Navigator initialRouteName="Login">
        <stack.Screen name="Login" component={Login} />
        <stack.Screen
          name="Main"
          component={Mainscreen}
          options={{headerShown: false}}
        />
        <stack.Screen name="CreateNew" component={CreateNew} />
      </stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
