import axios from 'axios';
import React from 'react';
import {Pressable, StyleSheet, Text, TextInput, View} from 'react-native';

const url = 'http://192.168.0.201:5000/expenses';

import {NativeStackScreenProps} from '@react-navigation/native-stack';
import {RootStackParamList} from '../App';

type CreateNewProps = NativeStackScreenProps<RootStackParamList>;

const CreateNew = ({navigation}: CreateNewProps) => {
  const [category, setCategory] = React.useState('');
  const [expenses, setExpenses] = React.useState('');
  const [mainData, setmainData] = React.useState([]);
  //   const onPressFunction = () => {
  //     console.log('okoko');
  //     navigation.navigate("Login")
  //   };
  const postData = async (data1: String, data2: String) => {
    try {
      const resp = await axios.post(url, {category: data1, expense: data2});
      const resp2 = await axios.get(url);
      console.log(resp2.data);
      console.log(resp.data);
    } catch (error) {
      console.log(error.response);
    }
  };
  return (
    <View>
      {/* <View style={Styles.mainDiv}>
        <Text style={s`text-white text-2xl`}>Navbar</Text>
      </View> */}
      <View style={Styles.container}>
        <View style={Styles.ImgContainer}></View>
        <View>
          <TextInput
            placeholder="Category"
            style={Styles.input}
            onChangeText={setCategory}
            value={category}></TextInput>
          <TextInput
            placeholder="Expenses"
            keyboardType="numeric"
            style={Styles.input}
            onChangeText={setExpenses}
            value={expenses}></TextInput>
        </View>
        <View style={Styles.buttonContainer}>
          <View style={Styles.buttonContainer2}>
            <Pressable
              onPress={() => {
                postData(category, expenses);
                // setTimeout(() => {
                //   navigation.navigate('Main');
                // }, 1000);
                navigation.navigate('Main');
              }}
              style={({pressed}) => [
                {backgroundColor: pressed ? 'white' : 'black'},
                Styles.button,
              ]}>
              <Text style={Styles.buttonText}>Add</Text>
            </Pressable>
          </View>
          <View style={Styles.buttonContainer2}>
            <Pressable
              onPress={() => {
                // postData(category, expenses);
                // setTimeout(() => {
                //   navigation.navigate('Main');
                // }, 1000);
                navigation.navigate('Main');
              }}
              style={({pressed}) => [
                {backgroundColor: pressed ? 'white' : 'black'},
                Styles.button,
              ]}>
              <Text style={Styles.buttonText}>Cancel</Text>
            </Pressable>
          </View>
        </View>
      </View>
    </View>
  );
};

export default CreateNew;

const Styles = StyleSheet.create({
  forgotPass: {
    fontWeight: 'bold',
    color: 'black',
    fontStyle: 'italic',
    fontSize: 15,
  },
  buttonText: {
    // backgroundColor: 'black',
    color: 'white',
    textAlign: 'center',
    // padding: 10,
    // borderRadius: 20,
    // fontSize: 15,
  },
  // buttonText: {
  //   // backgroundColor: 'black',
  //   color: 'white',
  //   textAlign: 'center',
  //   padding: 10,
  //   borderRadius: 20,
  //   fontSize: 15,
  // },
  buttonContainer: {
    alignItems: 'center',
    width: '100%',
    gap: 10,
  },
  buttonContainer2: {
    alignItems: 'center',
    width: '100%',
    paddingHorizontal: 10,
  },
  button: {
    width: '100%',
    borderRadius: 22,
    // backgroundColor: 'black',
    color: 'white',
    textAlign: 'center',
    padding: 13,
    fontSize: 15,
  },
  img: {
    width: '100%',
    resizeMode: 'stretch',
    marginBottom: 30,
  },
  ImgContainer: {
    alignItems: 'center',
    paddingHorizontal: 18,
  },
  container: {
    flexDirection: 'column',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
    padding: 10,
    gap: 20,
  },
  input: {
    height: 50,
    margin: 7,
    borderWidth: 2,
    borderRadius: 10,
    padding: 10,
    width: '100%',
    alignSelf: 'center',
  },
  inputText: {
    color: 'black',
    fontWeight: 'bold',
  },
});
