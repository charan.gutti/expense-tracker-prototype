import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  Image,
  Pressable,
  RefreshControl,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';

const url = 'http://192.168.0.201:5000/expenses';

const Notice = () => {
  const [refreshing, setRefreshing] = React.useState(false);
  const [mainData, setmainData] = useState([]);
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
    getData();
  }, []);
  const getData = async () => {
    try {
      const resp = await axios.get(url);
      setmainData(resp.data);
      console.log(mainData);
    } catch (error) {
      console.log(error.response);
    }
  };
  const deleteData = async (id: String) => {
    try {
      const resp = await axios.delete(url + '/' + id);
    } catch (error) {
      console.log(error.response);
    }
  };
  useEffect(() => {
    getData();
  }, []);
  return (
    <ScrollView
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }>
      <View style={styles.expenseContainer}>
        <Text style={styles.textHeading}>Reminders</Text>
        {mainData.map(data => (
          <View key={data._id} style={styles.separateExpenseContainer}>
            <Text style={styles.categoryText}>{data.category}</Text>
            <Text style={styles.timerText}>10min ago</Text>
            <View style={styles.imgMainContainer}>
              <Pressable
                style={styles.imgSubContainer}
                onPress={() => {
                  deleteData(data._id);
                  alert('Pressed');
                }}>
                <Image
                  style={styles.imgContainer}
                  source={require('../assets/checkicon.png')}
                />
              </Pressable>
              <Pressable style={styles.imgSubContainer}>
                <Image
                  style={styles.imgContainer}
                  source={require('../assets/crossIcon.png')}
                />
              </Pressable>
            </View>
          </View>
        ))}
      </View>
    </ScrollView>
  );
};

export default Notice;

const styles = StyleSheet.create({
  textContainer: {
    color: 'black',
    fontSize: 20,
    fontWeight: '900',
  },
  expenseContainer: {
    alignItems: 'center',
    gap: 10,
    padding: 20,
    marginTop: 12,
    // borderRadius: 2,
    // borderWidth: 2,
    // borderColor: 'black',
  },
  separateExpenseContainer: {
    borderWidth: 2,
    width: '100%',
    borderRadius: 7,
    padding: 12,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  categoryText: {
    color: 'black',
    textAlignVertical: 'center',
    fontSize: 17,
    fontWeight: '500',
  },
  timerText: {
    fontSize: 13,
    fontStyle: 'italic',
    textAlignVertical: 'center',
  },
  textHeading: {
    alignSelf: 'flex-start',
    fontWeight: 'bold',
    color: 'black',
    fontSize: 19,
  },
  imgContainer: {
    width: 50,
    height: 'auto',
    resizeMode: 'contain',
  },
  imgMainContainer: {
    flexDirection: 'row',
  },
  imgSubContainer: {
    flexDirection: 'row',
  },
});
