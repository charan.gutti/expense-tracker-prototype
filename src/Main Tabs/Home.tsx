import {NativeStackScreenProps} from '@react-navigation/native-stack';
import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  Image,
  Pressable,
  RefreshControl,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {RootStackParamList} from '../App';

const url = 'http://192.168.0.201:5000/expenses';
type Homeprops = NativeStackScreenProps<RootStackParamList>;

const Home = ({navigation}: Homeprops) => {
  const [show, setshow] = useState(false);
  const [mainData, setmainData] = useState([]);
  const [refreshing, setRefreshing] = React.useState(false);
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
    getData();
  }, []);
  const dropBoxdata = [
    {option: 'Create Goals'},
    {option: 'Create Goals'},
    {option: 'Create Goals'},
  ];
  const getData = async () => {
    try {
      const resp = await axios.get('http://192.168.0.201:5000/expenses');
      setmainData(resp.data);
      console.log(resp.data);
    } catch (error) {
      console.log(error.response);
    }
  };

  useEffect(() => {
    getData();
  }, []);
  return (
    <View>
      <View style={styles.topBar}>
        <View style={styles.imgContainer}>
          <Image
            style={styles.img}
            source={require('../assets/download.png')}></Image>
        </View>

        {show ? (
          <View style={styles.dropBox}>
            <Text style={styles.dropdownclose} onPress={() => setshow(false)}>
              X
            </Text>
            {dropBoxdata.map((data, id) => (
              <View key={id}>
                <Pressable
                  style={({pressed}) => [
                    {backgroundColor: pressed ? 'black' : 'white'},
                    styles.dropDownItems,
                  ]}>
                  <Text>{data.option}</Text>
                </Pressable>
              </View>
            ))}
          </View>
        ) : (
          <Text style={styles.tabText} onPress={() => setshow(true)}>
            Charan Gutti
          </Text>
        )}
      </View>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        <View style={styles.maincontainer}>
          <View style={styles.minicontainer}>
            <Text>Total Expenses</Text>
            <Text style={styles.textContainer}>Rs 2000</Text>
          </View>
          <View style={styles.minicontainer}>
            <Text>Daily Expense</Text>
            <Text style={styles.textContainer}>Rs 200</Text>
          </View>
        </View>
        <View style={styles.expenseContainer}>
          <Text style={styles.textHeading}>Recent Expenses</Text>
          {mainData.map((data, id) => (
            <View key={id} style={styles.separateExpenseContainer}>
              <Text style={styles.categoryText}>{data.category}</Text>
              <Text style={styles.timerText}>10min ago</Text>
              <Text style={styles.textContainer}>{` ${data.expense}`}</Text>
            </View>
          ))}
        </View>
      </ScrollView>
      <TouchableOpacity
        style={{
          borderWidth: 1,
          borderColor: 'white',
          alignItems: 'center',
          justifyContent: 'center',
          width: 70,
          position: 'absolute',
          top: 550,
          right: 20,
          height: 70,
          backgroundColor: 'black',
          borderRadius: 100,
        }}
        onPress={() => {
          // alert('Button is pressed');
          navigation.navigate('CreateNew');
        }}>
        <Text style={{color: 'white'}}>+</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  maincontainer: {
    paddingTop: 12,
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  minicontainer: {
    width: '40%',
    // justifyContent: 'center',
    // alignContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
    padding: 12,
    borderRadius: 12,
    borderWidth: 2,
    borderColor: 'black',
  },
  textContainer: {
    color: 'black',
    fontSize: 20,
    fontWeight: '900',
  },
  expenseContainer: {
    alignItems: 'center',
    gap: 10,
    padding: 20,
    marginTop: 12,
    // borderRadius: 2,
    // borderWidth: 2,
    // borderColor: 'black',
  },
  separateExpenseContainer: {
    borderWidth: 2,
    width: '100%',
    borderRadius: 7,
    padding: 12,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  categoryText: {
    color: 'black',
    textAlignVertical: 'center',
    fontSize: 17,
    fontWeight: '500',
  },
  timerText: {
    fontSize: 13,
    fontStyle: 'italic',
    textAlignVertical: 'center',
  },
  textHeading: {
    alignSelf: 'flex-start',
    fontWeight: 'bold',
    color: 'black',
    fontSize: 19,
  },
  imgContainer: {
    padding: 12,
  },
  topBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  img: {
    width: 125,
    height: 50,
    resizeMode: 'contain',
  },
  tabText: {
    padding: 20,
  },
  dropBox: {
    borderWidth: 2,
    position: 'absolute',
    top: 20,
    right: 20,
    padding: 10,
    zIndex: 10,
    backgroundColor: 'white',
    gap: 10,
    borderRadius: 10,
  },
  dropDownItems: {
    width: 200,
    alignItems: 'center',
    padding: 10,
  },
  dropdownclose: {
    alignSelf: 'flex-end',
  },
});
