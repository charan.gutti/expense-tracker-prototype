import {NativeStackScreenProps} from '@react-navigation/native-stack';
import React from 'react';
import {
  Image,
  Pressable,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import {RootStackParamList} from '../App';

type LoginProps = NativeStackScreenProps<RootStackParamList, 'Login'>;

const Login = ({navigation}: LoginProps) => {
  const [studentId, setStudentId] = React.useState('');
  const [password, setPassword] = React.useState('');

  //   const onPressFunction = () => {
  //     console.log('okoko');
  //     navigation.navigate("Login")
  //   };
  return (
    <View>
      {/* <View style={Styles.mainDiv}>
        <Text style={s`text-white text-2xl`}>Navbar</Text>
      </View> */}
      <View style={Styles.container}>
        <View style={Styles.ImgContainer}>
          <Image
            style={Styles.img}
            source={require('../assets/download.png')}></Image>
        </View>
        <View>
          <TextInput
            placeholder="Student ID"
            style={Styles.input}
            onChangeText={setStudentId}
            value={studentId}></TextInput>
          <TextInput
            placeholder="Password"
            style={Styles.input}
            onChangeText={setPassword}
            value={password}></TextInput>
        </View>
        <View style={Styles.buttonContainer}>
          <View style={Styles.buttonContainer2}>
            <Pressable
              onPress={() => navigation.navigate('Main')}
              style={({pressed}) => [
                {backgroundColor: pressed ? 'white' : 'black'},
                Styles.button,
              ]}>
              <Text style={Styles.buttonText}>Submit</Text>
            </Pressable>
          </View>

          <Text style={Styles.forgotPass}>Forgot Password</Text>
        </View>
      </View>
    </View>
  );
};

export default Login;

const Styles = StyleSheet.create({
  forgotPass: {
    fontWeight: 'bold',
    color: 'black',
    fontStyle: 'italic',
    fontSize: 15,
  },
  buttonText: {
    // backgroundColor: 'black',
    color: 'white',
    textAlign: 'center',
    // padding: 10,
    // borderRadius: 20,
    // fontSize: 15,
  },
  // buttonText: {
  //   // backgroundColor: 'black',
  //   color: 'white',
  //   textAlign: 'center',
  //   padding: 10,
  //   borderRadius: 20,
  //   fontSize: 15,
  // },
  buttonContainer: {
    alignItems: 'center',
    width: '100%',
    gap: 20,
  },
  buttonContainer2: {
    alignItems: 'center',
    width: '100%',
    paddingHorizontal: 10,
  },
  button: {
    width: '100%',
    borderRadius: 22,
    // backgroundColor: 'black',
    color: 'white',
    textAlign: 'center',
    padding: 13,
    fontSize: 15,
  },
  img: {
    width: '100%',
    resizeMode: 'stretch',
    marginBottom: 30,
  },
  ImgContainer: {
    alignItems: 'center',
    paddingHorizontal: 18,
  },
  container: {
    flexDirection: 'column',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
    padding: 10,
    gap: 20,
  },
  input: {
    height: 50,
    margin: 7,
    borderWidth: 2,
    borderRadius: 10,
    padding: 10,
    width: '100%',
    alignSelf: 'center',
  },
  inputText: {
    color: 'black',
    fontWeight: 'bold',
  },
});
