import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import React from 'react';
import {StyleSheet} from 'react-native';
import {RootStackParamList} from '../App';
import Home from './Home';
import Notice from './Notice';
import Settings from './Settings';
import Stats from './Stats';

type ParamList = {
  Home: undefined;
  Reminders: undefined;
  Settings: undefined;
  Logout: undefined;
  Stats: undefined;
};

const Tab = createBottomTabNavigator<ParamList>();

type stackProps = NativeStackScreenProps<RootStackParamList, 'Login'>;

const MainScreen = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={Home} options={{headerShown: false}} />
      <Tab.Screen
        name="Reminders"
        component={Notice}
        options={{headerShown: false}}
      />
      <Tab.Screen
        name="Stats"
        component={Stats}
        options={{headerShown: false}}
      />
      <Tab.Screen
        name="Settings"
        component={Settings}
        options={{headerShown: false}}
      />
    </Tab.Navigator>
  );
};

const Logout = ({navigation}: stackProps) => {
  navigation.pop();
};

export default MainScreen;

const styles = StyleSheet.create({
  button: {
    width: '100%',
    borderRadius: 22,
    // backgroundColor: 'black',
    color: 'white',
    textAlign: 'center',
    padding: 13,
    fontSize: 15,
  },
});
